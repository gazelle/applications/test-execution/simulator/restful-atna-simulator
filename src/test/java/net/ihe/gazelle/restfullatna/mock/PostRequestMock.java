package net.ihe.gazelle.restfullatna.mock;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import org.hl7.fhir.r4.model.AuditEvent;
import org.hl7.fhir.r4.model.Bundle;

import java.io.*;

public class PostRequestMock {

    public static Bundle getBundleAuditEvent() throws IOException {
        File file = new File("src/test/resources/templates/request/bundle_audit_event_example.json");
        return jsonBundleParsed(file);
    }

    public static Bundle getBundleAuditEventXml() throws IOException {
        File file = new File("src/test/resources/templates/request/bundle_audit_event_example.xml");
        return xmlBundleParsed(file);
    }

    public static Bundle getBundlePatient() throws IOException {
        File file = new File("src/test/resources/templates/request/bundle_patient_example.json");
        return jsonBundleParsed(file);
    }

    public static Bundle getBundleWithHttpPut() throws IOException {
        File file = new File("src/test/resources/templates/request/bundle_PUT_example.json");
        return jsonBundleParsed(file);
    }

    public static Bundle getBundleWithNoEntries() throws IOException {
        File file = new File("src/test/resources/templates/request/bundle_no_entries_example.json");
        return jsonBundleParsed(file);
    }

    public static Bundle getBundleWithNoResource() throws IOException {
        File file = new File("src/test/resources/templates/request/bundle_no_resource_example.json");
        return jsonBundleParsed(file);
    }

    public static Bundle getBundleWithNoType() throws IOException {
        File file = new File("src/test/resources/templates/request/bundle_no_type_example.json");
        return jsonBundleParsed(file);
    }

    public static AuditEvent getResourceAuditEvent() throws IOException {
        File file = new File("src/test/resources/templates/request/audit_resource_example.json");
        return jsonAuditEventParsed(file);
    }

    public static AuditEvent getResourceAuditEventEmpty() throws IOException {
        File file = new File("src/test/resources/templates/request/audit_resource_empty_example.json");
        return jsonAuditEventParsed(file);
    }

    public static AuditEvent getResourceAuditEventXml() throws IOException {
        File file = new File("src/test/resources/templates/request/audit_resource_example.xml");
        return xmlAuditEventParsed(file);
    }


    private static Bundle jsonBundleParsed(File file) throws IOException {
        return initializeJsonParser().parseResource(Bundle.class, initializeStringFromFile(file));
    }

    private static Bundle xmlBundleParsed(File file) throws IOException {
        return initializeXmlParser().parseResource(Bundle.class, initializeStringFromFile(file));
    }

    private static AuditEvent jsonAuditEventParsed(File file) throws IOException {
        return initializeJsonParser().parseResource(AuditEvent.class, initializeStringFromFile(file));
    }

    private static AuditEvent xmlAuditEventParsed(File file) throws IOException {
        return initializeXmlParser().parseResource(AuditEvent.class, initializeStringFromFile(file));
    }


    private static String initializeStringFromFile(File file) throws IOException {
        InputStream inputStream = new FileInputStream(file);
        InputStreamReader isReader = new InputStreamReader(inputStream);
        char[] charArray = new char[(int) file.length()];
        isReader.read(charArray);
        return new String(charArray);
    }

    private static IParser initializeJsonParser() {
        return FhirContext.forR4().newJsonParser();
    }

    private static IParser initializeXmlParser() {
        return FhirContext.forR4().newXmlParser();
    }
}
