package net.ihe.gazelle.restfulatna.validator;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.server.exceptions.InvalidRequestException;
import ca.uhn.fhir.validation.FhirValidator;
import ca.uhn.fhir.validation.IValidatorModule;
import ca.uhn.fhir.validation.SingleValidationMessage;
import ca.uhn.fhir.validation.ValidationResult;
import org.hl7.fhir.common.hapi.validation.validator.FhirInstanceValidator;
import org.hl7.fhir.r4.model.AuditEvent;
import org.hl7.fhir.r4.model.Bundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ResourceValidator {

    private static final Logger resourceValidatorLogger = LoggerFactory.getLogger(ResourceValidator.class);

    FhirContext ctx = FhirContext.forR4();
    IValidatorModule module = new FhirInstanceValidator(ctx);
    FhirValidator validator = ctx.newValidator().registerValidatorModule(module);

    public void validateResource(Bundle bundle){
        generateErrorMessages(validator.validateWithResult(bundle));
    }

    public void validateResource(AuditEvent auditEvent){
       generateErrorMessages(validator.validateWithResult(auditEvent));
    }

    public void generateErrorMessages(ValidationResult validationResult){
        if (!validationResult.isSuccessful()){
            StringBuilder bld = new StringBuilder();
            for (SingleValidationMessage next : validationResult.getMessages()) {
                if ("ERROR".equals(next.getSeverity().name())){
                    String errorMessage = " NEXT ISSUE: " + next.getSeverity() + " - " + next.getLocationString() + " - " + next.getMessage()+";";
                    bld.append(errorMessage);
                }
            }
            String totalMessageError=bld.toString();
            resourceValidatorLogger.error(totalMessageError);
            throw new InvalidRequestException(totalMessageError);
        }
    }



}
