package net.ihe.gazelle.restfulatna.model;

public final class ErrorMessages {

    private ErrorMessages() {
    }

    public static final String AUDIT_EVENT_ERROR_MESSAGE = "The FHIR Bundle Resource shall contain at least one FHIR AuditEvent Resource";
    public static final String BUNDLE_NULL_EMPTY = "Bundle is null or Empty";
    public static final String BUNDLE_TYPE_NULL = "RestfulATNA BundleType is null";
    public static final String BUNDLE_TYPE_ERROR = "RestfulATNA BundleType must be BATCH";
    public static final String LIST_ENTRIES_EMPTY_NULL = "List of Entries is null or Empty";
    public static final String MANDATORY_RESOURCE_MESSAGE = "A Resource is mandatory";
    public static final String AUDIT_EVENT_WITH_POST = "One of the entries must contain : an AuditEvent Resource AND a Request with a POST method";
}
