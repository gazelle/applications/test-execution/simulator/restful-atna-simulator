package net.ihe.gazelle.restfulatna.business;

import ca.uhn.fhir.rest.server.exceptions.InvalidRequestException;
import net.ihe.gazelle.restfulatna.model.ErrorMessages;
import net.ihe.gazelle.restfulatna.mustache.IMapToMustache;
import net.ihe.gazelle.restfulatna.mustache.MapToMustache;
import org.hl7.fhir.r4.model.AuditEvent;
import org.hl7.fhir.r4.model.Bundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.ihe.gazelle.restfulatna.model.ConstantValues;


import java.util.List;
import java.util.Map;

public class RestfulAtnaDispatcher {

    private final IMapToMustache iMapToMustache = new MapToMustache();
    private static final Logger auditEventLogger = LoggerFactory.getLogger(RestfulAtnaDispatcher.class);
    public Map<String, String> bundleToMap(Bundle iti20Bundle) {

        List<Bundle.BundleEntryComponent> listEntries = getEntriesFromBundle(iti20Bundle);
        boolean isAuditEventResourceWithPostRequest = false;
        AuditEvent auditEventFromRequest = new AuditEvent();

        for (Bundle.BundleEntryComponent entry : listEntries) {

            if (isThisAnPostRequest(entry) && isThisAnAuditEvent(entry)) {
                auditEventFromRequest = (AuditEvent) entry.getResource();
                isAuditEventResourceWithPostRequest = true;
                break;
            }
        }

        if (!isAuditEventResourceWithPostRequest) {
            auditEventLogger.error(ErrorMessages.AUDIT_EVENT_WITH_POST);
            throw new InvalidRequestException(ErrorMessages.AUDIT_EVENT_WITH_POST);
        } else {
            return iMapToMustache.mapReqAuditEventToMustache(auditEventFromRequest);
        }
    }

    public Map<String, String> resourceToMap(AuditEvent iti20Resource) {
        if (iti20Resource == null || iti20Resource.isEmpty() || !ConstantValues.AUDIT_EVENT.equals(iti20Resource.getResourceType().name())) {
            auditEventLogger.error(ErrorMessages.AUDIT_EVENT_ERROR_MESSAGE);
            throw new InvalidRequestException(ErrorMessages.AUDIT_EVENT_ERROR_MESSAGE);
        } else return iMapToMustache.mapReqAuditEventToMustache(iti20Resource);
    }

    private List<Bundle.BundleEntryComponent> getEntriesFromBundle(Bundle iti20Bundle) {
        if (iti20Bundle == null || iti20Bundle.isEmpty()) {
            auditEventLogger.error(ErrorMessages.BUNDLE_NULL_EMPTY);
            throw new InvalidRequestException(ErrorMessages.BUNDLE_NULL_EMPTY);
        } else if (iti20Bundle.getType() == null) {
            auditEventLogger.error(ErrorMessages.BUNDLE_TYPE_NULL);
            throw new InvalidRequestException(ErrorMessages.BUNDLE_TYPE_NULL);
        } else if (!Bundle.BundleType.BATCH.name().equalsIgnoreCase(iti20Bundle.getType().name())) {
            auditEventLogger.error(ErrorMessages.BUNDLE_TYPE_ERROR);
            throw new InvalidRequestException(ErrorMessages.BUNDLE_TYPE_ERROR);
        }
        return checkListEntry(iti20Bundle);
    }

    private List<Bundle.BundleEntryComponent> checkListEntry(Bundle iti20Bundle) {
        if (iti20Bundle.getEntry() == null || iti20Bundle.getEntry().isEmpty()) {
            auditEventLogger.error(ErrorMessages.LIST_ENTRIES_EMPTY_NULL);
            throw new InvalidRequestException(ErrorMessages.LIST_ENTRIES_EMPTY_NULL);
        }
        return iti20Bundle.getEntry();
    }

    private boolean isThisAnPostRequest(Bundle.BundleEntryComponent entry) {
        if (entry.getRequest().getMethod() != null) {
            return ConstantValues.POST.equals(entry.getRequest().getMethod().name());
        }
        return false;
    }

    private boolean isThisAnAuditEvent(Bundle.BundleEntryComponent entry) {
        if (entry.getResource() == null || entry.getResource().isEmpty() || entry.getResource().getResourceType() == null) {
            auditEventLogger.error(ErrorMessages.MANDATORY_RESOURCE_MESSAGE);
            throw new InvalidRequestException(ErrorMessages.MANDATORY_RESOURCE_MESSAGE);
        } else {
            return ConstantValues.AUDIT_EVENT.equals(entry.getResource().getResourceType().name());
        }
    }

}
