package net.ihe.gazelle.restfulatna.business;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import com.github.mustachejava.Mustache;
import net.ihe.gazelle.restfulatna.mustache.MustacheTemplating;
import org.hl7.fhir.r4.model.AuditEvent;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.OperationOutcome;

import java.io.StringWriter;
import java.util.Map;


public class ResponseManager {

    private final RestfulAtnaDispatcher restfulAtnaDispatcher = new RestfulAtnaDispatcher();
    private final MustacheTemplating mt = new MustacheTemplating();

    public Bundle createBundleReponse(Bundle iti20Bundle) {
        Map<String, String> map = restfulAtnaDispatcher.bundleToMap(iti20Bundle);
        return getBundleReponseFromMustache(map);
    }

    public AuditEvent createResourceReponse(AuditEvent iti20Resource) {
        Map<String, String> map = restfulAtnaDispatcher.resourceToMap(iti20Resource);
        return getResourceReponseFromMustache(map);
    }

    public OperationOutcome createOperationOutcome(Bundle iti20Bundle){
        Map<String, String> map = restfulAtnaDispatcher.bundleToMap(iti20Bundle);
        return getOperationOutcomeFromMustache(map);
    }

    public OperationOutcome createOperationOutcome(AuditEvent iti20AuditEvent){
        Map<String, String> map = restfulAtnaDispatcher.resourceToMap(iti20AuditEvent);
        return getOperationOutcomeFromMustache(map);
    }

    private Bundle getBundleReponseFromMustache(Map<String, String> map) {
        Mustache mu = mt.generateBundleResponseFromJson();
        IParser parser = getJsonParser();
        return parser.parseResource(Bundle.class, stringFromMustacheTemplate(mu, map));
    }

    private AuditEvent getResourceReponseFromMustache(Map<String, String> map) {
        Mustache mu = mt.generateResourceResponseFromJson();
        IParser parser = getJsonParser();
        return parser.parseResource(AuditEvent.class, stringFromMustacheTemplate(mu, map));
    }

    private OperationOutcome getOperationOutcomeFromMustache(Map<String, String> map) {
        Mustache mu = mt.generateOperationOutcomeResponseFromJson();
        IParser parser = getJsonParser();
        return parser.parseResource(OperationOutcome.class, stringFromMustacheTemplate(mu, map));
    }

    private String stringFromMustacheTemplate(Mustache mu, Map<String, String> map) {
        StringWriter writer = new StringWriter();
        mu.execute(writer, map);
        return writer.toString();
    }

    private IParser getJsonParser() {
        return FhirContext.forR4().newJsonParser();
    }

}
