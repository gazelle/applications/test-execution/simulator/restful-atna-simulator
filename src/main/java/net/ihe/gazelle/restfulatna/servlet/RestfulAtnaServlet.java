package net.ihe.gazelle.restfulatna.servlet;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.narrative.DefaultThymeleafNarrativeGenerator;
import ca.uhn.fhir.narrative.INarrativeGenerator;
import ca.uhn.fhir.rest.server.IResourceProvider;
import ca.uhn.fhir.rest.server.RestfulServer;
import ca.uhn.fhir.rest.server.exceptions.InternalErrorException;
import ca.uhn.fhir.rest.server.interceptor.*;
import net.ihe.gazelle.restfulatna.ws.provider.AuditEventBundleProvider;
import net.ihe.gazelle.restfulatna.ws.provider.AuditEventResourceProvider;

import javax.inject.Inject;
import javax.servlet.annotation.WebServlet;
import java.util.ArrayList;
import java.util.List;

@WebServlet(urlPatterns = {"/*"}, displayName = "FHIR Server for RESTful ATNA")
public class RestfulAtnaServlet extends RestfulServer {

    private static final long serialVersionUID = 1L;

    @Inject
    private AuditEventBundleProvider auditEventBundleProvider;
    @Inject
    private AuditEventResourceProvider auditEventResourceProvider;

    /**
     * Constructor
     */
    public RestfulAtnaServlet() {
        super(FhirContext.forR4());
    }

    /**
     * This method is called automatically when the
     * servlet is initializing.
     */
    @Override
    public void initialize() {
        /*
         * Two resource providers are defined. Each one handles a specific
         * type of resource.
         */
        List<IResourceProvider> providers = new ArrayList<>();
        providers.add(auditEventBundleProvider);
        providers.add(auditEventResourceProvider);
        setResourceProviders(providers);

        //creating an interceptor to log in console an error occurring
        LoggingInterceptor loggingInterceptor = new LoggingInterceptor();
        loggingInterceptor.setLoggerName("test.accesslog");
        loggingInterceptor.setMessageFormat("Source[$remoteAddr] Operation[${operationType}"
                + "${idOrResourceName}] UA[${requestHeader.user-agent}] Params[${requestParameters}]");

        registerInterceptor(loggingInterceptor);

        //creating an interceptor for special exception to dump the stack trace in the logs.
        ExceptionHandlingInterceptor exceptionInterceptor = new ExceptionHandlingInterceptor();
        exceptionInterceptor.setReturnStackTracesForExceptionTypes(InternalErrorException.class, NullPointerException.class);
        registerInterceptor(exceptionInterceptor);

        /*
         * Use a narrative generator. This is a completely optional step,
         * but can be useful as it causes HAPI to generate narratives for
         * resources which don't otherwise have one.
         */
        INarrativeGenerator narrativeGen = new DefaultThymeleafNarrativeGenerator();
        getFhirContext().setNarrativeGenerator(narrativeGen);

        /*
         * Use nice coloured HTML when a browser is used to request the content
         */
        registerInterceptor(new ResponseHighlighterInterceptor());

    }

}
