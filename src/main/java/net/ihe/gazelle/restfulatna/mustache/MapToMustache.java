package net.ihe.gazelle.restfulatna.mustache;

import net.ihe.gazelle.restfulatna.model.ConstantValues;
import org.hl7.fhir.r4.model.AuditEvent;

import java.util.HashMap;
import java.util.Map;

public class MapToMustache implements IMapToMustache {
    @Override
    public Map<String, String> mapReqAuditEventToMustache(AuditEvent auditEventFromRequest) {
        Map<String, String> map = new HashMap<>();

        map.put(ConstantValues.DATE, dateNow());

        map.put(ConstantValues.AUDIT_EVENT_ID, auditEventFromRequest.getId());

        map.put(ConstantValues.ID, "1");
        map.put(ConstantValues.ISSUE_CODE, "informational");
        map.put(ConstantValues.ISSUE_DETAILS_CODE, "All OK");
        map.put(ConstantValues.ISSUE_SEVERITY, "information");

        return map;
    }

}
