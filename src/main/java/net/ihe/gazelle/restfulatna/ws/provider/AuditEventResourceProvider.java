package net.ihe.gazelle.restfulatna.ws.provider;

import ca.uhn.fhir.rest.annotation.Create;
import ca.uhn.fhir.rest.annotation.ResourceParam;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.server.IResourceProvider;
import net.ihe.gazelle.restfulatna.business.ResponseManager;
import net.ihe.gazelle.restfulatna.validator.ResourceValidator;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.AuditEvent;
import org.hl7.fhir.r4.model.OperationOutcome;

import javax.inject.Named;


@Named("auditEventResourceProvider")
public class AuditEventResourceProvider implements IResourceProvider {
    private final ResponseManager responseManager = new ResponseManager();
    ResourceValidator rv = new ResourceValidator();


    @Override
    public Class<? extends IBaseResource> getResourceType() {
        return AuditEvent.class;
    }

    @Create()
    public MethodOutcome create(@ResourceParam AuditEvent iti20AuditEvent) {
        rv.validateResource(iti20AuditEvent);
        MethodOutcome mo = new MethodOutcome().setResource(responseManager.createResourceReponse(iti20AuditEvent));
        OperationOutcome oo = responseManager.createOperationOutcome(iti20AuditEvent);
        mo.setOperationOutcome(oo);
        return mo;
    }

}
