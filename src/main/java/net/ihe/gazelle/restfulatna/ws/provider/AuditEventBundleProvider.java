package net.ihe.gazelle.restfulatna.ws.provider;

import ca.uhn.fhir.rest.annotation.Create;
import ca.uhn.fhir.rest.annotation.ResourceParam;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.server.IResourceProvider;
import net.ihe.gazelle.restfulatna.business.ResponseManager;
import net.ihe.gazelle.restfulatna.validator.ResourceValidator;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.OperationOutcome;

import javax.inject.Named;


@Named("auditEventBundleProvider")
public class AuditEventBundleProvider implements IResourceProvider {
    private final ResponseManager responseManager = new ResponseManager();
    ResourceValidator rv = new ResourceValidator();

    @Override
    public Class<? extends IBaseResource> getResourceType() {
        return Bundle.class;
    }

    @Create()
    public MethodOutcome create(@ResourceParam Bundle iti20Bundle) {
        rv.validateResource(iti20Bundle);
        MethodOutcome mo = new MethodOutcome();
        OperationOutcome oo = responseManager.createOperationOutcome(iti20Bundle);
        Bundle r = responseManager.createBundleReponse(iti20Bundle);
        r.getEntry().get(1).getResponse().setOutcome(oo);
        mo.setResource(r);
        mo.setOperationOutcome(oo);
        return mo;
    }

}
